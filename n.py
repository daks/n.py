#!/usr/bin/python3
# -*- coding: utf-8 -*-
__version__ = "0.0.3"


# imports
import os, sys
import subprocess
from datetime import datetime

# global vars
folder = os.getenv("N_FOLDER", "$HOME/n/")
edit_tool = os.getenv("N_TOOL", "nano")
default_ext = os.getenv("N_DEFAULT_EXT", "txt")
default_shell = os.getenv("N_DEFAULT_SHELL", "sh")

header_help = """\033[1mn.py\033[0m − very simple note taking cli tool
\033[2mhomepage: https://git.bitmycode.com/sodimel/n
version {version}\033[0m
""".format(version=__version__)

help_txt = """{header_help}

\033[1mUsage:\033[0m
  ./n.py [OPTION] [str]

\033[1mArgs:\033[0m
  h, help, -h, --help       Display this (very long) help text in your shell/terminal.
  n, new, -n, --new         Create a new file. Default name is today : '%Y-%m-%d.txt'.
                            Add any str to create a named new file, e.g. './n.py c hi'
                            will create a file in 'N_FOLDER' or ~/n/ whith the name of
                            'hi.txt' using 'N_TOOL' or 'nano'. The command will open a
                            file if it already exist, effectively allowing you to edit
                            it. \033[2mCalling the 'n.py' script with no argument will launch
                            this exact same function. And entering an unrecognised arg
                            will create a new file named after the aforementioned arg.\033[0m
                            Add an other argument to define the extension of the file.
  s, search, -s, --search   Search a string in all files in 'N_FOLDER' or '~/n/', this
                            command sort the result with the latest opened file first.
  c, cat, -c, --cat         Print a file (if found) on plain ol' stdout. Useful if you
                            want to do some special grep after: just pipe the content!
                            Can print content of multiple files, sorted by most recent
                            updated first. Good if you want to cat your private diary.
                            You can use str substitution if you enter no arg, and then
                            just enter words or commands or an asterisk in the prompt.
  l, ls, -l, --ls           List files in 'N_FOLDER', last modified first, in one col.
                            Add argument to list only files where name is in '*name*'.

\033[1mEnv vars:\033[0m
  N_FOLDER                  This is the folder name where the script will save all the
                            files into. Will default to '~/n/' if the env var does not
                            exist. Please notice the trailing slash. You are in charge
                            of creating the folder if it does not exist: run (maybe) a
                            'mkdir ~/n' command before you run the n.py script a first
                            time, or else you will be experiencing an error, like this
                            one: \033[1m\033[97m\033[41m[ Directory '/home/me/aaaa' does not exist ]\033[0m (this is
                            the default result of using nano (which is the tool that's
                            used if no N_TOOL environment variable is found when using
                            n.py. Here is the exact command I ran in order to get that
                            previous output on my computer: 'N_FOLDER=~/aaaa/ ./n.py'.
                            A cool thing to do is to set it to something starting with
                            '$HOME/', like this: 'export N_FOLDER="$HOME/notes/"'. Its
                            really important to do this, in order to make autocomplete
                            like the one that's shared in this help work w/ any error.
  N_TOOL                    The tool used when creating/editing a note. The default is
                            nano, because it's simple to use and because a way to exit
                            is actually displayed on the bottom panel (\033[2myes, I refer to
                            this strange tool called vim\033[0m). You may use this var in the
                            case of your OS don't have nano, or maybe if you just want
                            to use another tool (maybe vim, emacs, geany, gedit, code,
                            subl or any other program that can edit simple txt files).
  N_DEFAULT_EXT             Extension to save files if none is provided. Default value
                            is 'txt', but personally I set it to 'md' because nano has
                            a very cool syntax highlightning for markdown all content.
  N_DEFAULT_SHELL           Default to 'sh'. N.py is launching an interactive shell: I
                            wanted to be able to see md content with color output, and
                            so I switched from default 'cat' command to 'bat' (that is
                            awesome). I needed to be able to make Python run a command
                            with the .zshrc (for my case) loaded, and so I updated the
                            script, and then created this brand new env var to let you
                            do whatever you want. This feature is now added in v0.0.3.

\033[1mAutocomplete:\033[0m
                            I'm not really into all this bash-based stuff, but you can
                            find a small script and some instruction by executing this
                            command: '\033[1mn autocomplete\033[0m', or '\033[1mpython3 n.py autocomplete\033[0m'.

\033[1mWhy another tool?\033[0m
                            I used a certain number of note taking programs and others
                            text editing apps and concepts. Here's a small list of the
                            tools & concepts: Joplin, Notable, a strange concept named
                            Johnny•Decimal, a (secret) gitlab wiki, some post-it tools
                            like the xubuntu post-it app named Xpad, and an other tool
                            named StickyNotes. But I didnt stay under any of the apps,
                            because they didn't had all the features that I wanted. So
                            I decided to create this tool after discovering this great
                            post (https://thesephist.com/posts/inc/) and discovering a
                            note taking tool named "inc" (stands for "incremental"). I
                            really liked the philosphy behind this tool, and I created
                            n.py to be very similar to inc. But n.py still differs for
                            these few reasons (this is indeed not an exhaustive list):
                            - the command line interface is different: you cant launch
                            a REPL, except when running './n.py s'(earch) without arg.
                            - the name of a note is not a number (like in inc), but is
                            the date of today (in the form '%Y-%m-%d.txt'). So it's (I
                            think) easier to retrieve content relative to a date, e.g.
                            "ah yes I wrote this thought yesterday! Lets search in the
                            file named '(\033[1m%Y-1\033[0m)-%m-%d.txt', and see if I can find it!".
                            - \033[1mthis tool is waayyy smaller than inc, and packages a lot
                            less features\033[0m than this other tool (for an example, it did
                            not comes with a REPL, an history of commands, or a single
                            file database. Don't expect it to replace it anytime soon!

\033[1mLinks of other tools/concepts:\033[0m
                           Joplin ----------------------------- https://joplinapp.org/
                           Notable ------------------------------ https://notable.app/
                           Johnny•Decimal ----------------- https://johnnydecimal.com/
                           Gitlab Wiki - https://docs.gitlab.com/ee/user/project/wiki/
                           Xpad --------------------------- https://launchpad.net/xpad
                           StickyNotes -------------------------- https://git.io/JwHnC
                           inc --------------------- https://github.com/thesephist/inc

\033[1mExamples:\033[0m
  Create file               $ python3 n.py
                            \033[2m'N_TOOL' 'N_FOLDER'2021/10/08.txt\033[0m

  Create named file         $ ./n.py n todo
                            \033[2m'N_TOOL' 'N_FOLDER'todo.txt\033[0m

  Create named file           $ n done
                            \033[2m'N_TOOL' 'N_FOLDER'done.txt\033[0m

  List all files            $ n l
                            \033[2mcd 'N_FOLDER'; if [ $(ls -A 2>/dev/null | wc -l) -ne 0 ]; \\
                            then ls --color -1t 'N_FOLDER'; else exit 1; fi\033[0m
                            done.txt
                            todo.txt
                            2021/10/08.txt

  Cat all files which       $ ./n.py cat todo
  is in *todo*              n cat todo
                            \033[2mcd 'N_FOLDER'; if [ $(ls -A *todo* 2>/dev/null | wc -l) -ne 0 ]; \\
                            then cat $(ls *todo*); else exit 1; fi\033[0m
                            TODO
                            - create cat command
                            - create ls command

  Cat files (prompt         $ n c
  with a *), error          What do you want to cat?
                            > *de
                            \033[2mcd 'N_FOLDER'; if [ $(ls -A **de* 2>/dev/null | wc -l) -ne 0 ]; \\
                            then cat $(ls **de*); else exit 1; fi\033[0m
                            No *de file found in 'N_FOLDER'.

  Cat files (prompt         $ n c
  with a *), success        What do you want to cat?
                            > *do
                            \033[2mcd 'N_FOLDER'; if [ $(ls -A **do* 2>/dev/null | wc -l) -ne 0 ]; \\
                            then cat $(ls **do*); else exit 1; fi\033[0m
                            DONE
                            - created a cat command, lets see if it works
                            TODO
                            - create cat command
                            - create ls command

  Search for "create"       $ python3 n.py s create
  string in all files       \033[2mcd 'N_FOLDER'; grep --color=ALWAYS "todo" $(ls -1t); exit $?;\033[0m
                            \033[2mtodo.txt\033[0m:- \033[1mcreate\033[0m cat command
                            \033[2mtodo.txt\033[0m:- \033[1mcreate\033[0m ls command
""".format(header_help=header_help)

# functions
def dim(t):
    return "\033[2m"+t+"\033[0m"


def print_and_execute(command):
    print(dim(command))
    print()
    return subprocess.call([default_shell, '-i', '-c', command])


def new_or_update(args):
    if args[0] in ["n", "new", "-n", "--new"]:
        name = datetime.now().strftime('%Y-%m-%d')
        name_ext = name + "." + default_ext
    else:
        name = args[0]
        if len(args) == 2:
            name_ext = name + "." + args[1]
        else:
            name_ext = name + "." + default_ext
    if os.path.isfile(folder + name):
        print_and_execute(edit_tool+" "+folder + name)
    else:
        print_and_execute(edit_tool+" "+folder + name_ext)


def search(args):
    if len(args) == 2:
        search_str = args[1]
    else:
        search_label = "What do you want to search?\n>"
        try:
            search_str = raw_input(search_label)
        except NameError:
            search_str = input(search_label)
    command = "cd "+folder+"; grep --color=ALWAYS \""+search_str+"\" $(ls -1t); exit $?;"
    output = print_and_execute(command)
    if ( isinstance(output, int) and output == 1 ) or not isinstance(output, int) and output.returncode == 1:
        print("No result found for "+search_str+" in "+folder+".")


def cat(args):
    if len(args) == 2:
        cat_filename = args[1]
    else:
        cat_label = "What do you want to cat?\n>"
        try:
            cat_filename = raw_input(cat_label)
        except NameError:
            cat_filename = input(cat_label)
    command = "cd "+folder+"; if [ $(ls -A *"+cat_filename+"* 2>/dev/null | wc -l) -ne 0 ]; then cat $(ls -t *"+cat_filename+"*); else exit 1; fi"
    output = print_and_execute(command)
    if ( isinstance(output, int) and output == 1 ) or not isinstance(output, int) and output.returncode == 1:
        print("No "+cat_filename+" file found in "+folder+".")


def ls(args):
    ls_filename = ""
    if len(args) == 2:
        ls_filename = args[1]
    command = "cd "+folder+"; if [ $(ls -A 2>/dev/null | wc -l) -ne 0 ]; then ls --color -1t "+folder +" | grep \""+ls_filename+"\"; else exit 1; fi"
    output = print_and_execute(command)
    if ( isinstance(output, int) and output == 1 ) or not isinstance(output, int) and output.returncode == 1:
        print("No file found in "+folder+".")


def main(args):
    if len(args):
        if args[0] in ["v", "version", "-v", "--version"]:
            print(header_help)
            exit()
        if args[0] in ["autocomplete"]:
            print("Here you go, add this in your ~/.(ba|z|...)shrc:\n")
            print("function _n_comp(){")
            print("    COMPREPLY=( $(compgen -W \"$(ls -C ${N_FOLDER:-\"$HOME/n/\"})\" -- ${COMP_WORDS[COMP_CWORD]} ) );")
            print("}")
            print("complete -F _n_comp n -o filenames -o nosort")
            print("\n\033[2mOnly tested using bash & zsh.\nAutocomplete only list files in 'N_FOLDER', feel free to hack it in order to add more features.\nCompletion works for the 'n' command, so be sure to add it to your path :)\033[0m")
            exit()
        if args[0] in ["s", "search", "-s", "--search"]:
            search(args)
        elif args[0] in ["h", "help", "-h", "--help"]:
            print(help_txt)
        elif args[0] in ["c", "cat", "-c", "--cat"]:
            cat(args)
        elif args[0] in ["l", "ls", "-l", "--ls"]:
            ls(args)
        else:
            new_or_update(args)
    else:
        new_or_update("n")


if __name__ == '__main__':
    main(sys.argv[1:])
